INC_DIR		= .
SRC_DIR		= .

M_NAMES		= 	atoi.c bzero.c calloc.c isalnum.c isalpha.c isascii.c \
				isdigit.c isprint.c itoa.c memccpy.c memchr.c memcmp.c \
				memcpy.c memmove.c memset.c putchar_fd.c putendl_fd.c \
				putnbr_fd.c putstr_fd.c split.c strchr.c strdup.c strjoin.c \
				strlcat.c strlcpy.c strlen.c strmapi.c strncmp.c strnstr.c \
				strrchr.c strtrim.c substr.c tolower.c toupper.c
M_SRCS		= $(addprefix $(SRC_DIR)/ft_, $(M_NAMES))

B_NAMES		= 	lstnew.c lstadd_front.c lstsize.c lstlast.c lstadd_back.c \
				lstdelone.c lstclear.c lstiter.c lstmap.c
B_SRCS		= $(addprefix $(SRC_DIR)/ft_, $(B_NAMES))

M_OBJS		= $(M_SRCS:.c=.o)
B_OBJS		= $(B_SRCS:.c=.o)

NAME		= libft.a

CC			= gcc
CFLAGS		= -Wall -Wextra -Werror

AR			= ar -rc

RM			= rm -f

RL			= ranlib

.c.o:
			$(CC) $(CFLAGS) -I $(INC_DIR) -c $< -o $(<:.c=.o)

$(NAME):	$(M_OBJS)
			$(AR) $(NAME) $(M_OBJS)
			$(RL) $(NAME)

all:		$(NAME)

bonus:		$(M_OBJS) $(B_OBJS)
			$(AR) $(NAME) $(M_OBJS) $(B_OBJS)
			$(RL) $(NAME)

clean:
			$(RM) $(M_OBJS) $(B_OBJS)

fclean:		clean
			$(RM) $(NAME)

re:			fclean all

.PHONY:		all bonus clean fclean re